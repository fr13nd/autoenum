#!/bin/bash
#createDirs.sh

stuff(){
	
	parentDir=$1
	mkdir -p "$parentDir"
	mkdir -p "$parentDir/scanning"
	mkdir -p "$parentDir/loot"
	touch "$parentDir/proof.txt"
	touch "$parentDir/writeup.txt"
	mkdir -p "$parentDir/exploit"
	mkdir -p "$parentDir/screenshots"
}

if [ "$1" != "" ]; then
	stuff $1
else
	echo "Missing argument"
	echo "Usage: ./createDirs [parent directory]"
fi

