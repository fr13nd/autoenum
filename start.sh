#!/bin/bash

#Usage: ./scan.sh [IP address] [output folder]
#Usage: ./createDirs [parent directory]

if [[ $# != 2 ]]; then
	echo "Run the script with & to background"
	echo "Usage: $0 [ip address range] [parent folder] &"
	echo ""
	echo "Example: $0 192.168.5.1-5 /root/Documents/hackthebox &"
	exit 1
else
	IP=$1
	FOLD=$2
	BASEIP=$(echo "$IP" | cut -d "-" -f 1)
	CUTIP=$(echo "$BASEIP" | rev | cut -d "." -f2- | rev)
	MINOCT=$(echo "$BASEIP" | cut -d "." -f 4)
	MAXOCT=$(echo "$IP" | cut -d "-" -f 2)
	echo "Base: $BASEIP"
	echo "Min: .$MINOCT"
	echo "Max: .$MAXOCT"
	for i in `seq "$MINOCT" "$MAXOCT"`;
		do
		echo "$CUTIP.$i"
		/root/Documents/scripts/createDirs.sh "$FOLD/$CUTIP.$i"
		/root/Documents/scripts/scan.sh "$CUTIP.$i" "$FOLD/$CUTIP.$i/scanning" &
	done

fi
