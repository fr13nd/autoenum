#!/bin/bash
#scan.sh

if [ $# != 2 ]; then
	echo 'Usage ./scan.sh [IP address] [output folder]'
	exit 1
fi

IP="$1"
#base_name="$2"
output="$2"
log="status.log"
initial='allPorts'
target='targeted'
ports='targetedPorts.txt'
echo -e "[#] Starting inital scan\n" >> "$output/$log"
echo "nmap -p- -n -vv \"$IP\" -oA \"$output/$initial\"" >> "$output/$log"
nmap -p- -n -vv "$IP" -oA "$output/$initial" >> "$output/$log"
echo -e "\n[#] Grepping ports..." >> "$output/$log"
cat "$output/$initial.xml" | grep "portid" | cut -d \" -f4 | tr '\n' ',' | sed 's/,$//' > "$output/$ports"
echo -e "\n[#] Starting final scan\n" >> "$output/$log"
targetPorts=$(cat "$output/$ports")
echo "nmap -vv -Pn -n -sC -sV --version-all -p$targetPorts -oA \"$output/$target\" \"$IP\" >> \"$output/$log\"" >> "$output/$log"
nmap -vv -Pn -n -sC -sV --version-all -p"$targetPorts" -oA "$output/$target" "$IP" >> "$output/$log"
echo -e "\n[!] Scanning complete" >> "$output/$log"
